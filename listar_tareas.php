<?php 
require_once("config/database.php");

if (!isset($_GET["tipo"])) {
    $filtro = "NULL";
} else {
    $tipo_tarea = $_GET["tipo"];
}

/*
if ($tipo_tarea == "abiertas") {
    $filtro = "NULL";
} else {
    $filtro = "NOT NULL";
}
*/

$tareas_abiertas_sql = "
    SELECT t.id,
           t.descripcion,
           solicitante.nombre as solicitante,
           asignado.nombre as asignado,
           t.prioridad,
           c.nombre as categoria,
           e.nombre as estado,
           t.fecha_creacion
    FROM tareas t
    INNER JOIN categorias c
       ON t.categoria_id = c.id
    INNER JOIN estados e
       ON t.estado_id = e.id
    INNER JOIN usuarios solicitante
       ON t.solicitante_id = solicitante.id
   INNER JOIN usuarios asignado
       ON t.solicitante_id = asignado.id
    WHERE fecha_fin IS $filtro
      AND asignado_id = 1
    ORDER BY t.id";

$tareas_abiertas = mysqli_query($conn, $tareas_abiertas_sql);
echo "
                    <ul style='list-style-type: none;'>";
while ($tarea = mysqli_fetch_array($tareas_abiertas)) {
    echo "
                        <li><a href='#' onclick='detalle_tarea(" . $tarea["id"] . ");'>" . $tarea["descripcion"] . "</a> 
                            <a href='#' title='Cerrar tarea' onclick='cerrar_tarea(" . $tarea["id"] . ");'><i class='fas fa-check'></i></a>
                            <div style='display: none;' class='detalle-tarea-". $tarea["id"] . "'>
                                <ul>
                                    <li>Creada: " . $tarea["fecha_creacion"] . "</li>
                                    <li>Creada por: " . $tarea["solicitante"] . "</li>
                                    <li>Asignada a: " . $tarea["asignado"] . "</li>
                                    <li>Categoría: " . $tarea["categoria"] . "</li>
                                    <li>Estado: " . $tarea["estado"] . "</li>
                                </ul>
                                <div class='opciones-tarea text-center'>
                                    <button type='button' class='btn btn-primary btn-sm'>Editar</button>
                                    <button type='button' class='btn btn-secondary btn-sm'>Borrar</button>
                                </div>
                            </div>
                        </li>";
}
echo "
                    </ul>";
?>