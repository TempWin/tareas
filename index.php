<?php 
require_once("config/database.php");
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Lista de tareas</title>

        <!-- Bootstrap core CSS -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Font Awesome -->
        <link href="vendor/fontawesome-5.1.0/css/all.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <style>
        body {
            padding-top: 54px;
        }

        #listado_tareas ul {
            padding-left: 0;
        }

        #listado_tareas ul li {
            padding: 10px 15px;
            border: 1px solid #dbdbdb;
            margin-bottom: 5px;
        }

        @media (min-width: 992px) {
            body {
              padding-top: 56px;
            }
        }

        .nueva_tarea {
            margin: 20px;
            padding: 20px;
        }

        </style>

    </head>

  <body onload="listar_tareas();">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div class="container">
            <a class="navbar-brand" href="#">Tareas</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Tereas abiertas <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Finalizadas</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <div class="col-lg-10 offset-lg-1" id="resultado" style="display:none;">
            </div>
            <div class="col-lg-10 offset-lg-1">
                <h2>Tareas para JM <a href="#" role="button" class="btn btn-default" onclick="nueva_tarea();"><i class="fas fa-plus-circle"></i></a></h2>

                <div class="nueva_tarea" style="display: none;">
                    <form id="form-tarea">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Descripción de la tarea</label>
                            <input type="text" class="form-control" id="descripcion" name="descripcion" aria-describedby="descripcion" placeholder="">
                        </div>
                        <div class="form-group row">
                            <label for="solicitante" class="col-sm-2 col-form-label">Solicitante</label>
                            <div class="col-sm-3">
                                <select name="solicitante_id" id="solicitante_id" class="form-control">
<?php 
$usuarios_sql = "
    SELECT *
    FROM usuarios";

$usuarios = mysqli_query($conn, $usuarios_sql);
while ($usuario = mysqli_fetch_array($usuarios)) {
    echo "
                                    <option value='" . $usuario["id"] . "'>" . $usuario["nombre"] . "</option>";
}
?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="exampleInputPassword1" class="col-sm-2 col-form-label">Asignado a</label>
                            <div class="col-sm-3">
                                <select name="asignado_id" id="asignado_id" class="form-control">
<?php 
$usuarios_sql = "
    SELECT *
    FROM usuarios";

$usuarios = mysqli_query($conn, $usuarios_sql);
while ($usuario = mysqli_fetch_array($usuarios)) {
    echo "
                                    <option value='" . $usuario["id"] . "'>" . $usuario["nombre"] . "</option>";
}
?>                                
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="prioridad" class="col-sm-2 col-form-label">Prioridad</label>
                            <div class="col-sm-2">
                                <select name="prioridad" id="prioridad" class="form-control">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="categoria" class="col-sm-2 col-form-label">Categoría</label>
                            <div class="col-sm-3">
                                <select name="categoria_id" id="categoria_id" class="form-control">
<?php 
$categorias_sql = "
    SELECT *
    FROM categorias";

$categorias = mysqli_query($conn, $categorias_sql);
while ($categoria = mysqli_fetch_array($categorias)) {
    echo "
                                    <option value='" . $categoria["id"] . "'>" . $categoria["nombre"] . "</option>";
}
?>                                
                                </select>
                            </div>
                        </div>
                        
                        <button type="button" class="btn btn-primary" onclick="insertar_tarea();">Añadir</button>
                    </form>
                </div> <!-- nueva tarea -->
                <div id="listado_tareas">
                </div> <!-- listado tareas -->
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script>
        function listar_tareas() {
            $("#listado_tareas").load("listar_tareas.php");
        }
        function detalle_tarea(id) {
            $(".detalle-tarea-" + id).toggle();
        }

        function nueva_tarea() {
            $(".nueva_tarea").toggle();
        }

        function cerrar_tarea(id) {
            $.ajax({
                data: {
                    "id": id
                },
                type: "post",
                url: "cerrar_tarea.php",
                beforeSend: function() {
                    console.log("Antes");
                    console.log("Tarea: " + id);
                },
                success: function(response) {
                    console.log("Bien!");
                    $("#resultado").toggle();
                    $("#resultado").html(response);
                    $("#resultado").delay(2000).fadeOut();
                    listar_tareas();
                },
                error: function() {
                    console.log("ERROR: cerrar_tarea()");
                }
            });
        }

        function insertar_tarea() {
            var datos = $("#form-tarea").serialize();

            $.ajax({
                data: datos,
                type: "post",
                url: "crear_tarea.php",
                beforeSend: function() {
                    console.log("Antes");
                    console.log(datos);
                },
                success: function(response) {
                    console.log("Bien!");
                    
                    $("#resultado").toggle();
                    $("#resultado").html(response);
                    $("#resultado").delay(2000).fadeOut();
                    $(".nueva_tarea").toggle();
                    listar_tareas();
                    

                },
                error: function() {
                    console.log("ERROR: insertar_tarea()");
                }
            });
        }
    </script>

  </body>

</html>
