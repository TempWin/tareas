<?php 
require_once("config/database.php");

$descripcion = $_POST["descripcion"];
$solicitante_id = $_POST["solicitante_id"];
$asignado_id = $_POST["asignado_id"];
$prioridad = $_POST["prioridad"];
$categoria_id = $_POST["categoria_id"];
$fecha_creacion = date("Y-m-d H:i:s");

$crear_tarea_sql = "INSERT INTO tareas 
                        (descripcion, solicitante_id, asignado_id, prioridad, categoria_id, fecha_creacion)
                    VALUES 
                        ('$descripcion', $solicitante_id, $asignado_id, $prioridad, $categoria_id, '$fecha_creacion')";

$crear_tarea = mysqli_query($conn, $crear_tarea_sql);

if (!$crear_tarea) {
    echo "ERROR: Insertar tarea";
} else {
    echo "
                <div class='alert alert-success' role='alert'>
                    <i class='far fa-thumbs-up'></i> Tarea creada.
                </div>";
}
?>