<?php 
require_once("config/database.php");

$id = $_POST["id"];
$fecha_fin = date("Y-m-d H:i:s");

$cerrar_tarea_sql = "UPDATE tareas 
                        SET fecha_fin = '$fecha_fin'
                      WHERE id = $id";

$cerrar_tarea = mysqli_query($conn, $cerrar_tarea_sql);

if (!$cerrar_tarea) {
    echo "ERROR: Cerrar tarea";
} else {
    echo "
                <div class='alert alert-success' role='alert'>
                    <i class='far fa-thumbs-up'></i> Tarea cerrada.
                </div>";
}
?>