<?php 
/* 
    ---------------------------------
    Configuración de la base de datos
    ---------------------------------

*/
$servidor = "";
$usuario = "";
$password = "";
$db = "";

// Crear conexión
$conn = mysqli_connect($servidor, $usuario, $password, $db);

// Check connection
if (!$conn) {
    die("Error en la conexión: " . mysqli_connect_error());
}

mysqli_set_charset($conn, "utf8");
?>